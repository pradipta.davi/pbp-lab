from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm
from django.http.response import HttpResponseRedirect


# Create your views here.
def index(request):
    notes = Note.objects.all().values()
    response = {"notes": notes}
    print(response)
    print(notes)
    return render(request, 'lab4_index.html', response)


def add_note(request):
    form = NoteForm(request.POST or None)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect('/lab-4/')

    response = {'form': form}
    return render(request, 'lab4_form.html', response)


def note_list(request):
    notes = Note.objects.all().values()
    response = {"notes": notes}
    return render(request, 'lab4_notelist.html', response)