from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from lab_1.models import Friend
from lab_3.forms import FriendForm
from django.http.response import HttpResponseRedirect


# Create your views here.
@login_required(login_url="/admin/login/")
def index(request):
    friends = Friend.objects.all().values()  # DONE Implement this
    response = {'friends': friends}
    print(response)
    print(friends)
    return render(request, 'lab3_index.html', response)


def add_friend(request):
    form = FriendForm(request.POST or None)
    if form.is_valid():
        form.save()
        return HttpResponseRedirect('/lab-3/')

    response = {'form': form}
    return render(request, 'lab3_form.html', response)
