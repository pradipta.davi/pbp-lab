1. Apakah perbedaan antara JSON dan XML?
   JSON:
   1. Sebuah data format.
   2. Menyimpan data dalam bentuk key dan value.
   3. Berbasis Javascript.
   4. Hanya mendukung datatype primitif, seperti string, integer, dan boolean.
   5. Mendukung datatype array.
   
   XML:
   1. Sebuah markup language.
   2. Mempunyai struktur seperti tree yang dimulai dari root, lalu branch, hingga berakhir pada leaves.
   3. Mendukung datatype kompleks, seperti citra, grafik, dan gambar.
   4. Tidak mendukung datatype array secara langsung.
   5. Syntax lebih rumit dibandingkan dengan Json.
   
2. Apakah perbedaan antara HTML dan XML?
   HTML:
   1. Mempunyai fokus dalam visualisasi data.
   2. Berbasis format.
   3. Case Insensitive.
   4. Sedikit toleran terhadap error kode..
   5. Berukuran lebih kecil dibanding XML.
   
   XML:
   1. Mempunyai fokus dalam data transfer.
   2. Berbasis konten.
   3. Case Sensitive.
   4. Tidak toleran terhadap error kode.
   5. Berukuran besar dibanding HTML.