import 'package:flutter/material.dart';
import 'package:lab_7/screens/obatpedia.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  static const String title = 'Covid Consult';

  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => MaterialApp(
    debugShowCheckedModeBanner: false,
    darkTheme: ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.purple,
        scaffoldBackgroundColor: const Color(0xFF383838),
    ),

    themeMode: ThemeMode.dark,
    title: title,
    home: const MainPage(),
  );
}

class MainPage extends StatefulWidget {
  final String title = 'Covid Consult';

  const MainPage({Key? key}) : super(key: key);



  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) => Scaffold(
    appBar: AppBar(
      title: Text(widget.title),
      backgroundColor: const Color(0xff131313),
    ),
    body: ListView(
      padding: const EdgeInsets.all(16),
      children: [
        Container(
          child:
          const Text('Obatpedia', textAlign: TextAlign.center,
            style:
            TextStyle(fontSize: 40,
                fontWeight: FontWeight.bold),
          ),
          margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
        ),
        Column(
          children: const <Widget>[
            SizedBox(height:20.0),
          ],
        ),
        Container(
          child:
          const Text('Search Medicine',textAlign: TextAlign.center,
            style:
            TextStyle(fontSize: 30,
            ),
          ),
          margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
        ),
        Container(
          child: TextField(
            decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                labelText: 'Search'),
          ),
          margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
        ),
        Container(
          padding: const EdgeInsets.all(15),
          decoration: BoxDecoration(color: const Color(0xff6B46C1),
              borderRadius: BorderRadius.circular(20)),
          child: FlatButton(
            onPressed: () {
              _showFullModal(context);
            },
            child:
            const Text( 'Add New Medicine',style: TextStyle(color: Colors.white,
                fontSize: 25),
            ),
          ),
          margin: const EdgeInsets.fromLTRB(0, 20, 0, 10),
        ),
      ],
    ),
    bottomNavigationBar: BottomNavigationBar(

      backgroundColor: const Color(0xff131313),
      unselectedItemColor: Colors.white,
      selectedItemColor: Colors.white,


      // type: BottomNavigationBarType.fixed,
      items: [
        BottomNavigationBarItem(
          backgroundColor: Theme.of(context).primaryColor,
          icon: const Icon(Icons.home),
          title: const Text('Home'),
        ),
        BottomNavigationBarItem(
          backgroundColor: Theme.of(context).primaryColor,
          icon: const Icon(Icons.medication),
          title: const Text('Obatpedia'),
        ),

      ],
    ),
  );

  Widget buildSearch() {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
    );
  }
}

_showFullModal(context) {
  showGeneralDialog(
    context: context,
    barrierDismissible: false, // should dialog be dismissed when tapped outside
    barrierLabel: "Modal", // label for barrier
    transitionDuration: Duration(milliseconds: 250), // how long it takes to popup dialog after button click
    pageBuilder: (_, __, ___) { // your widget implementation 
      return Scaffold(
          appBar: AppBar(
              backgroundColor: const Color.fromRGBO(20, 20, 20, 1),
              centerTitle: true,
              leading: IconButton(
                  icon: const Icon(
                    Icons.close,
                    color: Colors.white,
                  ),
                  onPressed: (){
                    Navigator.pop(context);
                  }
              ),
              title: const Text("Add Medicine" , style: TextStyle(fontFamily: 'Mulish', fontSize: 30, fontWeight: FontWeight.bold),),
              elevation: 0.0
          ),
          backgroundColor: Colors.black,
          body: Container(
            padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(
                  color: Color(0xfff8f8f8),
                  width: 1,
                ),
              ),
            ),
            child: Form(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 10),
                  TextFormField(
                    decoration: InputDecoration(
                      hintText: "Wonderdrug",
                      labelText: "Name:",
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
                    ),
                  ),
                  const SizedBox(height: 10),
                  TextFormField(
                    decoration: InputDecoration(
                      hintText: "Covid Begone",
                      labelText: "Description:",
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
                    ),
                  ),
                  const SizedBox(height: 10),
                  TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                      hintText: "Philosopher's Stone",
                      labelText: "Composition:",
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
                    ),
                  ),
                  const SizedBox(height: 10),
                  TextFormField(
                    keyboardType: TextInputType.phone,
                    decoration: InputDecoration(
                      hintText: "Consume once and be cured",
                      labelText: "Dosage and Instructions:",
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
                    ),
                  ),
                  TextFormField(
                    keyboardType: TextInputType.phone,
                    decoration: InputDecoration(
                      hintText: "Immortality",
                      labelText: "Side effects:",
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
                    ),
                  ),
                  const SizedBox(height: 20),
                  Row(mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                          width: 100.0,
                          height: 50.0,
                          child:
                          OutlinedButton(onPressed: () {Navigator.pop(context);},
                            style: OutlinedButton.styleFrom(
                              primary: Colors.white,
                              backgroundColor: Theme.of(context).primaryColor,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                              // side: const BorderSide(color: Color.fromRGBO(233, 216, 253, 1), width: 2.5)
                            ),
                            child: const Text("Submit", style: TextStyle(fontSize: 20, fontFamily: 'Mulish', fontWeight: FontWeight.w500, color: Colors.white),),
                          )
                      )
                    ],
                  )
                ],
              ),
            ),
          )
      );
    },
  );
}

